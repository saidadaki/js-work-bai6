function timSoNhoNhat(){
    var num = 0;
    var step = 1;
    while(num < 10000){
        num += step;
        step += 1;
    }
    document.getElementById("result__1").innerHTML = `Số nguyên dương nhỏ nhất là: ${step - 1}`;
    document.getElementById("result__1").style.display = "inline-block";
}

function tongDaySo(){
    var soDuoi = document.getElementById("soX").value * 1;
    var soTren = document.getElementById("soN").value * 1;
    if(soTren < 0){
        alert("Dữ liệu không hợp lệ");
    }
    var tongDaySo = 0
    for (var i = 1; i <= soTren; i++){
        tongDaySo += soDuoi**i ; 
    }
    document.getElementById("result__2").innerHTML = `Tổng dãy số là: ${tongDaySo}`;
    document.getElementById("result__2").style.display = "inline-block";
}

function tinhGiaiThua(){
    var soMu = document.getElementById("num__1").value * 1;
    var soGiaiThua = 1;
    
    for (var i=1; i<=soMu; i++){
        soGiaiThua *= i
    }
    document.getElementById("result__3").innerHTML = `Số ${soMu} có giai thừa là: ${soGiaiThua}`;
    document.getElementById("result__3").style.display = "inline-block";
}

function taoTheDiv(){
    var danhSachDiv = "";
    for (var i = 1; i<=10; i++){
        if(i % 2==0){
            danhSachDiv += `<div class="bg-danger text-white"> Div chẵn ${i}</div>`;
        }else{
            danhSachDiv += `<div class="bg-primary text-white"> Div lẻ ${i}</div>`;
        }
    }
    document.getElementById("result").innerHTML = danhSachDiv;
}


